import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';
import 'rxjs/add/operator/map'




@Injectable()
export class ApiService {

  public url: string;
  data:any = null;

  constructor( private _http: Http) {
    this.url = GLOBAL.url;


  }

  register(){
    return this._http.get(this.url + 'listadoProductos');
  }

  getProducts():Observable<any>{
      return this._http.get(this.url + 'listadoProductos');



  }
}
