import { Component, OnInit } from '@angular/core';
import {GLOBAL} from '../global';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.component.html',
  styleUrls: ['./galeria.component.css'],
  providers: [ ApiService ]
})
export class GaleriaComponent implements OnInit {
  data:any;

  constructor(private _ApiService: ApiService) { }

  ngOnInit() {
    this.data = this._ApiService.getProducts().map((res: Response) => res.json())
      .subscribe(data => {
        this.data = data;
        console.log(this.data);
      });

  }
}
