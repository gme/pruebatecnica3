import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterializeModule } from 'angular2-materialize';
import { AppComponent } from './app.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { HttpModule} from '@angular/http';


@NgModule({
  declarations: [
    AppComponent,
    GaleriaComponent

  ],
  imports: [
    BrowserModule,
    MaterializeModule,
    HttpModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
